package com.example.amit.nagarparishadnew;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

public class PropertyListActivity extends AppCompatActivity {
    private ArrayList<PropertyNoPojo> listProperty;
    private RecyclerView recyclerView;
    private PropertyListAdapter propertyListAdapter;
    private PropertyNoDatabaseHandler databaseHandler;
    private String searchBy, searchText;
    private TextView tvNoRecordsFound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("");
        // enabling action bar app icon and behaving it as toggle button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        databaseHandler = PropertyNoDatabaseHandler.getInstance(this);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        tvNoRecordsFound = (TextView) findViewById(R.id.tvNoRecordsFound);

        searchBy = getIntent().getStringExtra("searchBy");
        searchText = getIntent().getStringExtra("searchText");
        listProperty = new ArrayList<>();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        new SearchPropertDetailsTask().execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setAdapter() {
        propertyListAdapter = new PropertyListAdapter(listProperty, new PropertyListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(PropertyNoPojo propertyNoPojo) {
                PropertyDetailsDialogFragment fragment = PropertyDetailsDialogFragment.getInstance(propertyNoPojo);
                fragment.show(getSupportFragmentManager(), fragment.getClass().getName());
            }
        });
        recyclerView.setAdapter(propertyListAdapter);
    }

    class SearchPropertDetailsTask extends AsyncTask<Void, Void, Void> {
        private ProgressDialog progressDialog;

        public SearchPropertDetailsTask() {
            progressDialog = new ProgressDialog(PropertyListActivity.this);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Fetching Data...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            String searchByColumn = PropertyNoDatabaseHandler.PROPNO;

            if (searchBy.equalsIgnoreCase("PROPNO")) {
                searchByColumn = PropertyNoDatabaseHandler.PROPNO;
            } else if (searchBy.equalsIgnoreCase("OLDPROP")) {
                searchByColumn = PropertyNoDatabaseHandler.OLDPROP;
            } else if (searchBy.equalsIgnoreCase("PROP_OWNER")) {
                searchByColumn = PropertyNoDatabaseHandler.PROP_OWNER;

            }
            listProperty = databaseHandler.searchPropertyNoDetails(searchText, searchByColumn);//गावठाण
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();
            if (listProperty == null || listProperty.size() == 0)
                tvNoRecordsFound.setVisibility(View.VISIBLE);
            else
                setAdapter();
        }
    }
}
