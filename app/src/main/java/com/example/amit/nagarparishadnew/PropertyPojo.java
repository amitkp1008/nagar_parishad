package com.example.amit.nagarparishadnew;

/**
 * Created by Amit on 19/11/2017.
 */

public class PropertyPojo {
    private String NO, PM_PROPNO, PM_OLDPROPNO, PM_ASSE_FNAME, PM_ASSE_MNAME, PM_ASSE_LNAME, PM_ASSE_ADDRESS, PM_PROP_ADDRESS, PM_BILL_ADDRESS, PM_ASSE_EFNAME, PM_ASSE_EMNAME, PM_ASSE_ELNAME;

    public String getNO() {
        return NO;
    }

    public void setNO(String NO) {
        this.NO = NO;
    }

    public String getPM_PROPNO() {
        return PM_PROPNO;
    }

    public void setPM_PROPNO(String PM_PROPNO) {
        this.PM_PROPNO = PM_PROPNO;
    }

    public String getPM_OLDPROPNO() {
        return PM_OLDPROPNO;
    }

    public void setPM_OLDPROPNO(String PM_OLDPROPNO) {
        this.PM_OLDPROPNO = PM_OLDPROPNO;
    }

    public String getPM_ASSE_FNAME() {
        return PM_ASSE_FNAME;
    }

    public void setPM_ASSE_FNAME(String PM_ASSE_FNAME) {
        this.PM_ASSE_FNAME = PM_ASSE_FNAME;
    }

    public String getPM_ASSE_MNAME() {
        return PM_ASSE_MNAME;
    }

    public void setPM_ASSE_MNAME(String PM_ASSE_MNAME) {
        this.PM_ASSE_MNAME = PM_ASSE_MNAME;
    }

    public String getPM_ASSE_LNAME() {
        return PM_ASSE_LNAME;
    }

    public void setPM_ASSE_LNAME(String PM_ASSE_LNAME) {
        this.PM_ASSE_LNAME = PM_ASSE_LNAME;
    }

    public String getPM_ASSE_ADDRESS() {
        return PM_ASSE_ADDRESS;
    }

    public void setPM_ASSE_ADDRESS(String PM_ASSE_ADDRESS) {
        this.PM_ASSE_ADDRESS = PM_ASSE_ADDRESS;
    }

    public String getPM_PROP_ADDRESS() {
        return PM_PROP_ADDRESS;
    }

    public void setPM_PROP_ADDRESS(String PM_PROP_ADDRESS) {
        this.PM_PROP_ADDRESS = PM_PROP_ADDRESS;
    }

    public String getPM_BILL_ADDRESS() {
        return PM_BILL_ADDRESS;
    }

    public void setPM_BILL_ADDRESS(String PM_BILL_ADDRESS) {
        this.PM_BILL_ADDRESS = PM_BILL_ADDRESS;
    }

    public String getPM_ASSE_EFNAME() {
        return PM_ASSE_EFNAME;
    }

    public void setPM_ASSE_EFNAME(String PM_ASSE_EFNAME) {
        this.PM_ASSE_EFNAME = PM_ASSE_EFNAME;
    }

    public String getPM_ASSE_EMNAME() {
        return PM_ASSE_EMNAME;
    }

    public void setPM_ASSE_EMNAME(String PM_ASSE_EMNAME) {
        this.PM_ASSE_EMNAME = PM_ASSE_EMNAME;
    }

    public String getPM_ASSE_ELNAME() {
        return PM_ASSE_ELNAME;
    }

    public void setPM_ASSE_ELNAME(String PM_ASSE_ELNAME) {
        this.PM_ASSE_ELNAME = PM_ASSE_ELNAME;
    }
}
