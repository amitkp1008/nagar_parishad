package com.example.amit.nagarparishadnew;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by psk on 12/10/2017.
 */

public class PropertyDetailsDialogFragment extends DialogFragment {

    public static PropertyDetailsDialogFragment getInstance(PropertyNoPojo propertyNoPojo) {
        PropertyDetailsDialogFragment fragment = new PropertyDetailsDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("propertyNoPojo", propertyNoPojo);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        setCancelable(false);
        PropertyNoPojo propertyNoPojo = (PropertyNoPojo) getArguments().getSerializable("propertyNoPojo");
        View view = inflater.inflate(R.layout.property_list_item_details, container, false);
        TextView tvRn = (TextView) view.findViewById(R.id.tvRn);
        TextView tvPlvl6 = (TextView) view.findViewById(R.id.tvPlvl6);
        TextView tvPlvl9 = (TextView) view.findViewById(R.id.tvPlvl9);
        TextView tvPlvl10 = (TextView) view.findViewById(R.id.tvPlvl10);
        TextView tvPropNo = (TextView) view.findViewById(R.id.tvPropNo);
        TextView tvOldPropNo = (TextView) view.findViewById(R.id.tvOldPropNo);
        TextView tvFlatNo = (TextView) view.findViewById(R.id.tvFlatNo);
        TextView tvAddr = (TextView) view.findViewById(R.id.tvAddr);
        TextView tvPropOwner = (TextView) view.findViewById(R.id.tvPropOwner);
        TextView tvCpdValue = (TextView) view.findViewById(R.id.tvCpdValue);
        TextView tvCurr = (TextView) view.findViewById(R.id.tvCurr);
        TextView tvArr = (TextView) view.findViewById(R.id.tvArr);
        TextView tvTotal = (TextView) view.findViewById(R.id.tvTotal);
        Button btnClose = (Button) view.findViewById(R.id.btnClose);

        tvRn.setText(propertyNoPojo.getRn());
        tvPlvl6.setText(propertyNoPojo.getpLvl6());
        tvPlvl9.setText(propertyNoPojo.getpLvl9());
        tvPlvl10.setText(propertyNoPojo.getpLvl10());
        tvPropNo.setText(propertyNoPojo.getPropNo());
        tvOldPropNo.setText(propertyNoPojo.getOldPropNo());
        tvFlatNo.setText(propertyNoPojo.getFlatNo());
        tvAddr.setText(propertyNoPojo.getAddr());
        tvPropOwner.setText(propertyNoPojo.getPropOwner());
        tvCpdValue.setText(propertyNoPojo.getCpdValue());
        tvCurr.setText(propertyNoPojo.getCurr());
        tvArr.setText(propertyNoPojo.getArr());
        tvTotal.setText(propertyNoPojo.getTotal());

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        return view;
    }
}
