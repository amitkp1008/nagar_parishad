package com.example.amit.nagarparishadnew;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Amit on 19/11/2017.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final String database_name = "ParishadDb";
    private static final int database_version = 1;


    public DatabaseHandler(Context context) {
        super(context, database_name, null, database_version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(PropertyNoDatabaseHandler.CREATE_PROPERTY_NO_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + PropertyNoDatabaseHandler.PROPERTY_NO_TABLE);
        onCreate(sqLiteDatabase);
    }
}
