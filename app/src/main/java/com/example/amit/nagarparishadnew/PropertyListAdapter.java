package com.example.amit.nagarparishadnew;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Amit on 21/11/2017.
 */


class PropertyListAdapter extends RecyclerView.Adapter<PropertyListAdapter.MyViewHolder> {

    private ArrayList<PropertyNoPojo> propertyList;
    private final OnItemClickListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvPropNo, tvOldPropNo, tvName;

        private MyViewHolder(View view) {
            super(view);
            tvPropNo = (TextView) view.findViewById(R.id.tvPropNo);
            tvOldPropNo = (TextView) view.findViewById(R.id.tvOldPropNo);
            tvName = (TextView) view.findViewById(R.id.tvName);
        }

        public void bind(final PropertyNoPojo propertyNoPojo, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(propertyNoPojo);
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(PropertyNoPojo item);
    }

    public PropertyListAdapter(ArrayList<PropertyNoPojo> propertyList,OnItemClickListener listener) {
        this.propertyList = propertyList;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.property_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        PropertyNoPojo propertyPojo = propertyList.get(position);
        holder.tvPropNo.setText(propertyPojo.getPropNo());
        holder.tvOldPropNo.setText(propertyPojo.getOldPropNo());
        holder.tvName.setText(propertyPojo.getPropOwner());

        holder.bind(propertyList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return propertyList.size();
    }

}
