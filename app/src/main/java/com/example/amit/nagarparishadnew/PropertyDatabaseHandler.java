package com.example.amit.nagarparishadnew;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by psk on 11/25/2017.
 */

public class PropertyDatabaseHandler {
    public static final String TABLE_NAME = "PropertyTable";

    public static final String SR_NO = "SR_NO";
    public static final String PM_PROPNO = "PM_PROPNO";
    public static final String PM_OLDPROPNO = "PM_OLDPROPNO";
    public static final String PM_ASSE_FNAME = "PM_ASSE_FNAME";
    public static final String PM_ASSE_MNAME = "PM_ASSE_MNAME";
    public static final String PM_ASSE_LNAME = "PM_ASSE_LNAME";
    public static final String PM_ASSE_ADDRESS = "PM_ASSE_ADDRESS";
    public static final String PM_PROP_ADDRESS = "PM_PROP_ADDRESS";
    public static final String PM_BILL_ADDRESS = "PM_BILL_ADDRESS";
    public static final String PM_ASSE_EFNAME = "PM_ASSE_EFNAME";
    public static final String PM_ASSE_EMNAME = "PM_ASSE_EMNAME";
    public static final String PM_ASSE_ELNAME = "PM_ASSE_ELNAME";

    public static final String CREATE_PROPERTY_TABLE = "CREATE TABLE " + TABLE_NAME + " ( SR_NO Integer, PM_PROPNO Text, PM_OLDPROPNO Text,PM_ASSE_FNAME Text,PM_ASSE_MNAME Text,PM_ASSE_LNAME Text,"
            + "PM_ASSE_ADDRESS Text,PM_PROP_ADDRESS Text,PM_BILL_ADDRESS Text,PM_ASSE_EFNAME Text,PM_ASSE_EMNAME Text,PM_ASSE_ELNAME Text )";
    private static PropertyDatabaseHandler propertyDatabaseHandler;
    private DatabaseHandler databaseHandler;

    private PropertyDatabaseHandler(Context context) {
        databaseHandler = new DatabaseHandler(context);
    }

    public static PropertyDatabaseHandler getInstance(Context context) {
        if (propertyDatabaseHandler == null) {
            propertyDatabaseHandler = new PropertyDatabaseHandler(context);
        }
        return propertyDatabaseHandler;
    }

    public void insertProperty(ArrayList<PropertyPojo> propertyList) {
        SQLiteDatabase db = databaseHandler.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            for (PropertyPojo propertyPojo : propertyList) {

                values.put(SR_NO, propertyPojo.getNO());
                values.put(PM_PROPNO, propertyPojo.getPM_PROPNO());
                values.put(PM_OLDPROPNO, propertyPojo.getPM_OLDPROPNO());
                values.put(PM_ASSE_FNAME, propertyPojo.getPM_ASSE_FNAME());
                values.put(PM_ASSE_MNAME, propertyPojo.getPM_ASSE_MNAME());
                values.put(PM_ASSE_LNAME, propertyPojo.getPM_ASSE_LNAME());
                values.put(PM_ASSE_ADDRESS, propertyPojo.getPM_ASSE_ADDRESS());
                values.put(PM_PROP_ADDRESS, propertyPojo.getPM_PROP_ADDRESS());
                values.put(PM_BILL_ADDRESS, propertyPojo.getPM_BILL_ADDRESS());
                values.put(PM_ASSE_EFNAME, propertyPojo.getPM_ASSE_EFNAME());
                values.put(PM_ASSE_EMNAME, propertyPojo.getPM_ASSE_EMNAME());
                values.put(PM_ASSE_ELNAME, propertyPojo.getPM_ASSE_ELNAME());

                db.insert(TABLE_NAME, null, values);
            }

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            db.close();
        }

    }

    public ArrayList<PropertyPojo> getAllPropertyDetails() {
        ArrayList<PropertyPojo> listProperty = new ArrayList<>();
        SQLiteDatabase db = databaseHandler.getReadableDatabase();
        //Cursor cursor=db.query(TABLE_NAME,null,null,null,null,null,null);
        String sql = "select * from " + TABLE_NAME;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.getCount() != 0) {
            cursor.moveToFirst();
            do {
                PropertyPojo propertyPojo = new PropertyPojo();
                String no = cursor.getString(cursor.getColumnIndex(SR_NO));
                String pm_propno = cursor.getString(cursor.getColumnIndex(PM_PROPNO));
                String pm_old_propno = cursor.getString(cursor.getColumnIndex(PM_OLDPROPNO));
                String pm_asse_fname = cursor.getString(cursor.getColumnIndex(PM_ASSE_FNAME));
                String pm_asse_mname = cursor.getString(cursor.getColumnIndex(PM_ASSE_MNAME));
                String pm_asse_lname = cursor.getString(cursor.getColumnIndex(PM_ASSE_LNAME));
                String pm_asse_address = cursor.getString(cursor.getColumnIndex(PM_ASSE_ADDRESS));
                String pm_prop_address = cursor.getString(cursor.getColumnIndex(PM_PROP_ADDRESS));
                String pm_bill_address = cursor.getString(cursor.getColumnIndex(PM_BILL_ADDRESS));
                String pm_asse_e_fname = cursor.getString(cursor.getColumnIndex(PM_ASSE_EFNAME));
                String pm_asse_e_mname = cursor.getString(cursor.getColumnIndex(PM_ASSE_EMNAME));
                String pm_asse_e_lname = cursor.getString(cursor.getColumnIndex(PM_ASSE_ELNAME));

                propertyPojo.setNO(no);
                propertyPojo.setPM_PROPNO(pm_propno);
                propertyPojo.setPM_OLDPROPNO(pm_old_propno);
                propertyPojo.setPM_ASSE_FNAME(pm_asse_fname);
                propertyPojo.setPM_ASSE_MNAME(pm_asse_mname);
                propertyPojo.setPM_ASSE_LNAME(pm_asse_lname);
                propertyPojo.setPM_ASSE_ADDRESS(pm_asse_address);
                propertyPojo.setPM_PROP_ADDRESS(pm_prop_address);
                propertyPojo.setPM_BILL_ADDRESS(pm_bill_address);
                propertyPojo.setPM_ASSE_EFNAME(pm_asse_e_fname);
                propertyPojo.setPM_ASSE_EMNAME(pm_asse_e_mname);
                propertyPojo.setPM_ASSE_ELNAME(pm_asse_e_lname);

                listProperty.add(propertyPojo);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return listProperty;
    }

    public ArrayList<PropertyPojo> searchPropertyDetails(String searchText, String searchBy) {
        ArrayList<PropertyPojo> listProperty = new ArrayList<>();
        SQLiteDatabase db = databaseHandler.getReadableDatabase();
        String sql = "";
//Cursor cursor=db.query(TABLE_NAME,null,searchBy+" LIKE '%?%'",new String[]{searchText},null,null,null);
        if (searchBy.equalsIgnoreCase("PM_PROPNO")) {

            sql = "select * from " + TABLE_NAME + " where " + PM_PROPNO + " LIKE '%" + searchText + "%'";

        } else if (searchBy.equalsIgnoreCase("PM_OLDPROPNO")) {

            sql = "select * from " + TABLE_NAME + " where " + PM_OLDPROPNO + " LIKE '%" + searchText + "%'";

        } else if (searchBy.equalsIgnoreCase("PM_ASSE_FNAME")) {

            sql = "select * from " + TABLE_NAME + " where " + PM_ASSE_FNAME + " LIKE '%" + searchText + "%'";

        } else if (searchBy.equalsIgnoreCase("PM_ASSE_MNAME")) {

            sql = "select * from " + TABLE_NAME + " where " + PM_ASSE_MNAME + " LIKE '%" + searchText + "%'";

        } else if (searchBy.equalsIgnoreCase("PM_ASSE_LNAME")) {

            sql = "select * from " + TABLE_NAME + " where " + PM_ASSE_LNAME + " LIKE '%" + searchText + "%'";

        } else if (searchBy.equalsIgnoreCase("PM_ASSE_EFNAME")) {

            sql = "select * from " + TABLE_NAME + " where " + PM_ASSE_EFNAME + " LIKE '%" + searchText + "%'";

        } else if (searchBy.equalsIgnoreCase("PM_ASSE_EMNAME")) {

            sql = "select * from " + TABLE_NAME + " where " + PM_ASSE_EMNAME + " LIKE '%" + searchText + "%'";

        } else if (searchBy.equalsIgnoreCase("PM_ASSE_ELNAME")) {

            sql = "select * from " + TABLE_NAME + " where " + PM_ASSE_ELNAME + " LIKE '%" + searchText + "%'";

        }

        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.getCount() != 0) {
            cursor.moveToFirst();
            do {
                PropertyPojo propertyPojo = new PropertyPojo();
                String no = cursor.getString(cursor.getColumnIndex(SR_NO));
                String pm_propno = cursor.getString(cursor.getColumnIndex(PM_PROPNO));
                String pm_old_propno = cursor.getString(cursor.getColumnIndex(PM_OLDPROPNO));
                String pm_asse_fname = cursor.getString(cursor.getColumnIndex(PM_ASSE_FNAME));
                String pm_asse_mname = cursor.getString(cursor.getColumnIndex(PM_ASSE_MNAME));
                String pm_asse_lname = cursor.getString(cursor.getColumnIndex(PM_ASSE_LNAME));
                String pm_asse_address = cursor.getString(cursor.getColumnIndex(PM_ASSE_ADDRESS));
                String pm_prop_address = cursor.getString(cursor.getColumnIndex(PM_PROP_ADDRESS));
                String pm_bill_address = cursor.getString(cursor.getColumnIndex(PM_BILL_ADDRESS));
                String pm_asse_e_fname = cursor.getString(cursor.getColumnIndex(PM_ASSE_EFNAME));
                String pm_asse_e_mname = cursor.getString(cursor.getColumnIndex(PM_ASSE_EMNAME));
                String pm_asse_e_lname = cursor.getString(cursor.getColumnIndex(PM_ASSE_ELNAME));

                propertyPojo.setNO(no);
                propertyPojo.setPM_PROPNO(pm_propno);
                propertyPojo.setPM_OLDPROPNO(pm_old_propno);
                propertyPojo.setPM_ASSE_FNAME(pm_asse_fname);
                propertyPojo.setPM_ASSE_MNAME(pm_asse_mname);
                propertyPojo.setPM_ASSE_LNAME(pm_asse_lname);
                propertyPojo.setPM_ASSE_ADDRESS(pm_asse_address);
                propertyPojo.setPM_PROP_ADDRESS(pm_prop_address);
                propertyPojo.setPM_BILL_ADDRESS(pm_bill_address);
                propertyPojo.setPM_ASSE_EFNAME(pm_asse_e_fname);
                propertyPojo.setPM_ASSE_EMNAME(pm_asse_e_mname);
                propertyPojo.setPM_ASSE_ELNAME(pm_asse_e_lname);

                listProperty.add(propertyPojo);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return listProperty;
    }
}
