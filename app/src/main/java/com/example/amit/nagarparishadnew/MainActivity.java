package com.example.amit.nagarparishadnew;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Layout;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.io.InputStream;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private PropertyNoDatabaseHandler databaseHandler;
    private Button searchButton;
    private Spinner spinner_search;
    private String selectedSpinnerItem = "PROPNO";
    private EditText editSearchText;
    private String searchText = "";
    private Spinner switchLang;
    private CustomKeyboardView mKeyboardView;
    private Keyboard mKeyboard;
    private ArrayAdapter<CharSequence> langAdapter;
    private TextInputLayout tilSearchText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        databaseHandler = PropertyNoDatabaseHandler.getInstance(this);
        searchButton = (Button) findViewById(R.id.search_button);
        spinner_search = (Spinner) findViewById(R.id.spinner_search);
        editSearchText = (EditText) findViewById(R.id.editSearchText);
        tilSearchText = (TextInputLayout) findViewById(R.id.tilSearchText);
        new FetchPropertyDataAsyncTask(this).execute();

        switchLang = (Spinner) findViewById(R.id.switchLang);
        mKeyboardView = (CustomKeyboardView) findViewById(R.id.keyboardView);

        langAdapter = ArrayAdapter.createFromResource(this, R.array.switchLangArr, android.R.layout.simple_spinner_item);
        langAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        switchLang.setAdapter(langAdapter);

        /**
         * Set Keyboard on selection
         */
        switchLang.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectKeyboard();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        editSearchText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.editSearchText) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        editSearchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                editSearchText.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValidate()) {
                    searchText = editSearchText.getText().toString();
                    Intent intent = new Intent(MainActivity.this, PropertyListActivity.class);
                    intent.putExtra("searchBy", selectedSpinnerItem);
                    intent.putExtra("searchText", searchText);
                    startActivity(intent);
                }
            }
        });

        spinner_search.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedSpinnerItem = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(editSearchText.getWindowToken(), 0);
    }

    private boolean isValidate() {
        if (editSearchText.getText().toString().length() <= 0) {
            editSearchText.setError("Enter search text");
            return false;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mKeyboardView.setVisibility(View.GONE);
    }

    public void selectKeyboard() {

        // Do not show the preview balloons
        mKeyboardView.setPreviewEnabled(false);

        if (switchLang.getSelectedItemPosition() != 0) {
            hideSoftKeyboard(MainActivity.this);

            if (switchLang.getSelectedItemPosition() == 2) {
                mKeyboard = new Keyboard(MainActivity.this, R.xml
                        .kbd_hin1);
                showKeyboardWithAnimation();
                mKeyboardView.setVisibility(View.VISIBLE);
                mKeyboardView.setKeyboard(mKeyboard);

            } else if (switchLang.getSelectedItemPosition() == 1) {
                mKeyboard =
                        new Keyboard(MainActivity.this, R.xml.kbd_mar1);
                showKeyboardWithAnimation();
                mKeyboardView.setVisibility(View.VISIBLE);
                mKeyboardView.setKeyboard(mKeyboard);
            }

            mKeyboardView.setOnKeyboardActionListener(new BasicOnKeyboardActionListener(
                    MainActivity.this,
                    editSearchText,
                    mKeyboardView));

            editSearchText.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    selectKeyboard();

                    switch (event.getAction()) {
                        case MotionEvent.ACTION_UP:
                            Layout layout = ((EditText) v).getLayout();
                            float x = event.getX() + editSearchText.getScrollX();
                            float y = event.getY() + editSearchText.getScrollY();
                            int line = layout.getLineForVertical((int) y);

                            int offset = layout.getOffsetForHorizontal(line, x);
                            if (offset > 0)
                                if (x > layout.getLineMax(0))
                                    editSearchText
                                            .setSelection(offset);     // touch was at end of text
                                else
                                    editSearchText.setSelection(offset - 1);

                            editSearchText.setCursorVisible(true);
                            break;
                    }
                    return true;
                }
            });
        } else {
            mKeyboardView.setVisibility(View.GONE);

            //Show Default Keyboard
            InputMethodManager imm =
                    (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(editSearchText, 0);
            editSearchText.setOnTouchListener(null);
        }
    }

    private void showKeyboardWithAnimation() {
        if (mKeyboardView.getVisibility() == View.GONE) {
            Animation animation = AnimationUtils
                    .loadAnimation(MainActivity.this,
                            R.anim.slide_from_bottom);
            mKeyboardView.showWithAnimation(animation);
        }
    }


    public class BasicOnKeyboardActionListener implements KeyboardView.OnKeyboardActionListener {

        EditText editText;
        CustomKeyboardView displayKeyboardView;
        private Activity mTargetActivity;

        public BasicOnKeyboardActionListener(Activity targetActivity, EditText editText,
                                             CustomKeyboardView
                                                     displayKeyboardView) {
            mTargetActivity = targetActivity;
            this.editText = editText;
            this.displayKeyboardView = displayKeyboardView;
        }

        @Override
        public void swipeUp() {
            // TODO Auto-generated method stub

        }

        @Override
        public void swipeRight() {
            // TODO Auto-generated method stub

        }

        @Override
        public void swipeLeft() {
            // TODO Auto-generated method stub

        }

        @Override
        public void swipeDown() {
            // TODO Auto-generated method stub

        }

        @Override
        public void onText(CharSequence text) {
            // TODO Auto-generated method stub
            int cursorPosition = editText.getSelectionEnd();
            String before = editText.getText().toString().substring(0, cursorPosition);
            String after = editText.getText().toString().substring(cursorPosition);
            editText.setText(before + text + after);
            editText.setSelection(cursorPosition + 1);
        }

        @Override
        public void onRelease(int primaryCode) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onPress(int primaryCode) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onKey(int primaryCode, int[] keyCodes) {
            switch (primaryCode) {
                case 66:
                case 67:
                    long eventTime = System.currentTimeMillis();
                    KeyEvent event =
                            new KeyEvent(eventTime, eventTime, KeyEvent.ACTION_DOWN, primaryCode, 0, 0, 0, 0,
                                    KeyEvent.FLAG_SOFT_KEYBOARD | KeyEvent.FLAG_KEEP_TOUCH_MODE);

                    mTargetActivity.dispatchKeyEvent(event);
                    break;
                case -106:
                    displayKeyboardView
                            .setKeyboard(new Keyboard(mTargetActivity, R.xml.kbd_hin2));
                    break;
                case -107:
                    displayKeyboardView
                            .setKeyboard(new Keyboard(mTargetActivity, R.xml.kbd_hin1));
                    break;
                case -110:
                    displayKeyboardView.setKeyboard(new Keyboard(mTargetActivity, R.xml.kbd_mar2));
                    break;
                case -111:
                    displayKeyboardView.setKeyboard(new Keyboard(mTargetActivity, R.xml.kbd_mar1));
                    break;
                default:
                    break;
            }
        }
    }

    private void fetchData() {
        try {

            InputStream inputStream = getResources().getAssets().open("parishad.csv");
            CSVReader csvFile = new CSVReader(inputStream);
            ArrayList<String[]> dataList = csvFile.read();

            ArrayList<PropertyPojo> propertyList = new ArrayList<>();

            for (String[] propertyData : dataList) {
//            itemArrayAdapter.add(propertyData);
                PropertyPojo propertyPojo = new PropertyPojo();
                propertyPojo.setNO(propertyData[0]);
                propertyPojo.setPM_PROPNO(propertyData[1]);
                propertyPojo.setPM_OLDPROPNO(propertyData[2]);
                propertyPojo.setPM_ASSE_FNAME(propertyData[3]);
                propertyPojo.setPM_ASSE_MNAME(propertyData[4]);
                propertyPojo.setPM_ASSE_LNAME(propertyData[5]);
                propertyPojo.setPM_ASSE_ADDRESS(propertyData[6]);
                propertyPojo.setPM_PROP_ADDRESS(propertyData[7]);
                propertyPojo.setPM_BILL_ADDRESS(propertyData[8]);
                propertyPojo.setPM_ASSE_EFNAME(propertyData[9]);
                propertyPojo.setPM_ASSE_EMNAME(propertyData[10]);
                propertyPojo.setPM_ASSE_ELNAME(propertyData[11]);

                propertyList.add(propertyPojo);
            }
//            databaseHandler.insertProperty(propertyList);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
