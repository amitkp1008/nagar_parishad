package com.example.amit.nagarparishadnew;

import java.io.Serializable;

/**
 * Created by psk on 11/25/2017.
 */

public class PropertyNoPojo implements Serializable {

    private String srNo, rn, pLvl6, pLvl9, pLvl10, propNo, oldPropNo, flatNo, addr, propOwner, cpdValue, curr, arr, total;

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getRn() {
        return rn;
    }

    public void setRn(String rn) {
        this.rn = rn;
    }

    public String getpLvl6() {
        return pLvl6;
    }

    public void setpLvl6(String pLvl6) {
        this.pLvl6 = pLvl6;
    }

    public String getpLvl9() {
        return pLvl9;
    }

    public void setpLvl9(String pLvl9) {
        this.pLvl9 = pLvl9;
    }

    public String getpLvl10() {
        return pLvl10;
    }

    public void setpLvl10(String pLvl10) {
        this.pLvl10 = pLvl10;
    }

    public String getPropNo() {
        return propNo;
    }

    public void setPropNo(String propNo) {
        this.propNo = propNo;
    }

    public String getOldPropNo() {
        return oldPropNo;
    }

    public void setOldPropNo(String oldPropNo) {
        this.oldPropNo = oldPropNo;
    }

    public String getFlatNo() {
        return flatNo;
    }

    public void setFlatNo(String flatNo) {
        this.flatNo = flatNo;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getPropOwner() {
        return propOwner;
    }

    public void setPropOwner(String propOwner) {
        this.propOwner = propOwner;
    }

    public String getCpdValue() {
        return cpdValue;
    }

    public void setCpdValue(String cpdValue) {
        this.cpdValue = cpdValue;
    }

    public String getCurr() {
        return curr;
    }

    public void setCurr(String curr) {
        this.curr = curr;
    }

    public String getArr() {
        return arr;
    }

    public void setArr(String arr) {
        this.arr = arr;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
