package com.example.amit.nagarparishadnew;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by psk on 12/10/2017.
 */

public class FetchPropertyDataAsyncTask extends AsyncTask<Void, Void, Boolean> {
    private Activity activity;
    private PropertyNoDatabaseHandler propertyNoDatabaseHandler;
    private ProgressDialog progressDialog;

    public FetchPropertyDataAsyncTask(Activity activity) {
        this.activity = activity;
        propertyNoDatabaseHandler = PropertyNoDatabaseHandler.getInstance(activity);
        progressDialog = new ProgressDialog(activity);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog.setMessage("Fetching Data...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        try {
            propertyNoDatabaseHandler.deleteAll();
            InputStream inputStream = activity.getResources().getAssets().open("nagar_parishad.csv");
            CSVReader csvFile = new CSVReader(inputStream);
            ArrayList<String[]> dataList = csvFile.read();

            ArrayList<PropertyNoPojo> propertyList = new ArrayList<>();

            for (String[] propertyData : dataList) {
                PropertyNoPojo propertyPojo = new PropertyNoPojo();
                propertyPojo.setSrNo(propertyData[0]);
                propertyPojo.setRn(propertyData[1]);
                propertyPojo.setpLvl6(propertyData[2]);
                propertyPojo.setpLvl9(propertyData[3]);
                propertyPojo.setpLvl10(propertyData[4]);
                propertyPojo.setPropNo(propertyData[5]);
                propertyPojo.setOldPropNo(propertyData[6]);
                propertyPojo.setFlatNo(propertyData[7]);
                propertyPojo.setAddr(propertyData[8]);
                propertyPojo.setPropOwner(propertyData[9]);
                propertyPojo.setCpdValue(propertyData[10]);
                propertyPojo.setCurr(propertyData[11]);
                propertyPojo.setArr(propertyData[12]);
                propertyPojo.setTotal(propertyData[13]);
                propertyList.add(propertyPojo);
            }
            propertyNoDatabaseHandler.insertPropertyNo(propertyList);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    protected void onPostExecute(Boolean isLoaded) {
        super.onPostExecute(isLoaded);
        progressDialog.dismiss();
        if (!isLoaded) {
            showErrorDialog();
        }
    }

    private void showErrorDialog() {
        new AlertDialog.Builder(activity)
                .setCancelable(false)
                .setTitle("Something Went Wrong")
                .setMessage("Unable to read file.Try Again?")
                .setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        new FetchPropertyDataAsyncTask(activity).execute();
                    }
                })
                .setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        activity.finish();
                    }
                }).show();
    }
}
