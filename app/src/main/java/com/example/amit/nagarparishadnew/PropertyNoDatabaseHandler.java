package com.example.amit.nagarparishadnew;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by psk on 11/25/2017.
 */

public class PropertyNoDatabaseHandler {
    public static final String PROPERTY_NO_TABLE = "PropertyNoTable";

    public static final String SR_NO = "SR_NO";
    public static final String RN = "RN";
    public static final String P_LVL6 = "P_LVL6";
    public static final String P_LVL9 = "P_LVL9";
    public static final String P_LVL10 = "P_LVL10";
    public static final String PROPNO = "PROPNO";
    public static final String OLDPROP = "OLDPROP";
    public static final String FLAT_NO = "FLAT_NO";
    public static final String ADDR = "ADDR";
    public static final String PROP_OWNER = "PROP_OWNER";
    public static final String CPD_VALUE = "CPD_VALUE";
    public static final String CURR = "CURR";
    public static final String ARR = "ARR";
    public static final String TOTAL = "TOTAL";

    public static final String CREATE_PROPERTY_NO_TABLE = "CREATE TABLE " + PROPERTY_NO_TABLE + " ( " + SR_NO + " Integer, " + RN + " Text, " + P_LVL6 + " Text," +
            P_LVL9 + " Text," + P_LVL10 + " Text," + PROPNO + " Text," + OLDPROP + " Text," + FLAT_NO + " Text," + ADDR + " Text," + PROP_OWNER + " Text," +
            CPD_VALUE + " Text," + CURR + " Text," + ARR + " Text," + TOTAL + " Text )";
    private static PropertyNoDatabaseHandler propertyNoDatabaseHandler;
    private DatabaseHandler databaseHandler;

    private PropertyNoDatabaseHandler(Context context) {
        databaseHandler = new DatabaseHandler(context);
    }

    public static PropertyNoDatabaseHandler getInstance(Context context) {
        if (propertyNoDatabaseHandler == null) {
            propertyNoDatabaseHandler = new PropertyNoDatabaseHandler(context);
        }
        return propertyNoDatabaseHandler;
    }

    public void insertPropertyNo(ArrayList<PropertyNoPojo> propertyList) {
        SQLiteDatabase db = databaseHandler.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            for (PropertyNoPojo propertyPojo : propertyList) {

                values.put(SR_NO, propertyPojo.getSrNo());
                values.put(RN, propertyPojo.getRn());
                values.put(P_LVL6, propertyPojo.getpLvl6());
                values.put(P_LVL9, propertyPojo.getpLvl9());
                values.put(P_LVL10, propertyPojo.getpLvl10());
                values.put(PROPNO, propertyPojo.getPropNo());
                values.put(OLDPROP, propertyPojo.getOldPropNo());
                values.put(FLAT_NO, propertyPojo.getFlatNo());
                values.put(ADDR, propertyPojo.getAddr());
                values.put(PROP_OWNER, propertyPojo.getPropOwner());
                values.put(CPD_VALUE, propertyPojo.getCpdValue());
                values.put(CURR, propertyPojo.getCurr());
                values.put(ARR, propertyPojo.getArr());
                values.put(TOTAL, propertyPojo.getTotal());

                db.insert(PROPERTY_NO_TABLE, null, values);
            }

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            db.close();
        }

    }

    public ArrayList<PropertyNoPojo> getAllPropertyNoDetails() {
        ArrayList<PropertyNoPojo> listProperty = new ArrayList<>();
        SQLiteDatabase db = databaseHandler.getReadableDatabase();
        //Cursor cursor=db.query(TABLE_NAME,null,null,null,null,null,null);
        String sql = "select * from " + PROPERTY_NO_TABLE;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.getCount() != 0) {
            cursor.moveToFirst();
            do {

                PropertyNoPojo propertyPojo = new PropertyNoPojo();
                String srNo = cursor.getString(cursor.getColumnIndex(SR_NO));
                String rn = cursor.getString(cursor.getColumnIndex(RN));
                String pLvl6 = cursor.getString(cursor.getColumnIndex(P_LVL6));
                String pLvl9 = cursor.getString(cursor.getColumnIndex(P_LVL9));
                String pLvl10 = cursor.getString(cursor.getColumnIndex(P_LVL10));
                String propNo = cursor.getString(cursor.getColumnIndex(PROPNO));
                String oldProp = cursor.getString(cursor.getColumnIndex(OLDPROP));
                String flatNo = cursor.getString(cursor.getColumnIndex(FLAT_NO));
                String addr = cursor.getString(cursor.getColumnIndex(ADDR));
                String propOwner = cursor.getString(cursor.getColumnIndex(PROP_OWNER));
                String cpdValue = cursor.getString(cursor.getColumnIndex(CPD_VALUE));
                String curr = cursor.getString(cursor.getColumnIndex(CURR));
                String arr = cursor.getString(cursor.getColumnIndex(ARR));
                String total = cursor.getString(cursor.getColumnIndex(TOTAL));

                propertyPojo.setSrNo(srNo);
                propertyPojo.setRn(rn);
                propertyPojo.setpLvl6(pLvl6);
                propertyPojo.setpLvl9(pLvl9);
                propertyPojo.setpLvl10(pLvl10);
                propertyPojo.setPropNo(propNo);
                propertyPojo.setOldPropNo(oldProp);
                propertyPojo.setFlatNo(flatNo);
                propertyPojo.setAddr(addr);
                propertyPojo.setPropOwner(propOwner);
                propertyPojo.setCpdValue(cpdValue);
                propertyPojo.setCurr(curr);
                propertyPojo.setArr(arr);
                propertyPojo.setTotal(total);

                listProperty.add(propertyPojo);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return listProperty;
    }

    public ArrayList<PropertyNoPojo> searchPropertyNoDetails(String searchText, String columnName) {
        ArrayList<PropertyNoPojo> listProperty = new ArrayList<>();
        SQLiteDatabase db = databaseHandler.getReadableDatabase();
        String sql = "";

        Cursor cursor = db.query(PROPERTY_NO_TABLE, null, columnName + " LIKE '%" + searchText + "%'", null, null, null, null);

        if (cursor.getCount() != 0) {
            cursor.moveToFirst();
            do {
                PropertyNoPojo propertyPojo = new PropertyNoPojo();
                String srNo = cursor.getString(cursor.getColumnIndex(SR_NO));
                String rn = cursor.getString(cursor.getColumnIndex(RN));
                String pLvl6 = cursor.getString(cursor.getColumnIndex(P_LVL6));
                String pLvl9 = cursor.getString(cursor.getColumnIndex(P_LVL9));
                String pLvl10 = cursor.getString(cursor.getColumnIndex(P_LVL10));
                String propNo = cursor.getString(cursor.getColumnIndex(PROPNO));
                String oldProp = cursor.getString(cursor.getColumnIndex(OLDPROP));
                String flatNo = cursor.getString(cursor.getColumnIndex(FLAT_NO));
                String addr = cursor.getString(cursor.getColumnIndex(ADDR));
                String propOwner = cursor.getString(cursor.getColumnIndex(PROP_OWNER));
                String cpdValue = cursor.getString(cursor.getColumnIndex(CPD_VALUE));
                String curr = cursor.getString(cursor.getColumnIndex(CURR));
                String arr = cursor.getString(cursor.getColumnIndex(ARR));
                String total = cursor.getString(cursor.getColumnIndex(TOTAL));

                propertyPojo.setSrNo(srNo);
                propertyPojo.setRn(rn);
                propertyPojo.setpLvl6(pLvl6);
                propertyPojo.setpLvl9(pLvl9);
                propertyPojo.setpLvl10(pLvl10);
                propertyPojo.setPropNo(propNo);
                propertyPojo.setOldPropNo(oldProp);
                propertyPojo.setFlatNo(flatNo);
                propertyPojo.setAddr(addr);
                propertyPojo.setPropOwner(propOwner);
                propertyPojo.setCpdValue(cpdValue);
                propertyPojo.setCurr(curr);
                propertyPojo.setArr(arr);
                propertyPojo.setTotal(total);

                listProperty.add(propertyPojo);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return listProperty;
    }

    public void deleteAll() {
        SQLiteDatabase db = databaseHandler.getWritableDatabase();
        db.delete(PROPERTY_NO_TABLE, null, null);
        db.close();
    }
}
